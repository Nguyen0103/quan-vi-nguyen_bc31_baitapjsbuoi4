/**
 * Bài 1
 * 
 * b1: input
 * khai báo biến ngày công , tiền công;
 * 
 * b2: xử lí
 * lấy ngày công x tiền công
 * 
 * b3: output
 * trả kết quả bằng console.log
 * sủ dụng innerHTML để hiển thị lên trình duyệt
 */
function caculateSalary() {
        var wage = document.getElementById('txt-wage').value * 1;
        console.log("wage:",wage)

        var workday = document.getElementById('txt-work-day').value * 1
        console.log("workday: ",workday)

        var clacsalary = wage * workday
        console.log("Salary",clacsalary)
    
        document.getElementById('salary').innerHTML = `<p>Lương nhận: ${clacsalary}</p>`
}
 
 /**
  * Bài 2
  * 
  * b1: input
  * khai báo 5 biến và gán giá trị cho 5 biến
  * 
  * b2: xử lí
  * Cộng các biến lại với nhau chia cho 5
  * 
  * b3: output
  * trả kết quả bằng console.log
  * sủ dụng innerHTML để hiển thị lên trình duyệt
  */
function clacavgrage() {
   var num1 = document.getElementById('Num1').value * 1
   var num2 = document.getElementById('Num2').value * 1
   var num3 = document.getElementById('Num3').value * 1
   var num4 = document.getElementById('Num4').value * 1
   var num5 = document.getElementById('Num5').value * 1
   console.log("số thứ 1:",num1 ,"số thứ 2:",num2 ,"số thứ 3:",num3 ,"số thứ 4:",num4, "số thứ 5:",num5)

   var average = (num1 + num2 + num3 + num4 + num5) / 5
   console.log("Trung bình cộng: ",average)

  document.querySelector('.average').innerHTML = `<p>Trung bình cộng: ${average}</p>`
}
 
 /**
  * Bài 3
  * 
  * b1: input
  * Tạo biến cho tiền tệ USD và tiền tệ VND
  * 
  * b2: xử lí
  *  Lấy Biến USD x Biến VND
  * 
  * b3: output
  * trả kết quả bằng console.log
  * sủ dụng innerHTML để hiển thị lên trình duyệt
  */
function convertUSD(){
   var inputUsd = document.getElementById('input-USD').value * 1
   var vnMoney = 23500
   console.log(inputUsd)

   var convert = inputUsd * vnMoney
   console.log(convert);

   document.querySelector('.output-VND').innerHTML = `<p>${new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(convert)}</p>`
}

 /**
  * Bài 4
  * 
  * b1: input
  * tạo 2 biến Dài, rộng và gán giá trị cho 2 biến
  * 
  * b2: xử lí
  * Tính diện tích hình chữ nhật : S = Long*width
  * Tính chu vi hình chữ nhật : P = (Long*width)*2
  * 
  * b3: output
  * trả kết quả bằng console.log
  * sủ dụng innerHTML để hiển thị lên trình duyệt
  */
 function clacRectangle() {
   var clacRectangleLong = document.getElementById('long').value * 1
   var clacRectangleWidth = document.getElementById('width').value * 1
    console.log("Long",clacRectangleLong, "Width",clacRectangleWidth)

  var clacRectangleS = clacRectangleLong * clacRectangleWidth 
  var clacRectangleP = (clacRectangleLong + clacRectangleWidth) * 2
  console.log("diện tích:",clacRectangleS,"chu vi:",clacRectangleP)
  
  document.querySelector('.clacRectangle').innerHTML = `<p>Diện tích: ${clacRectangleS}cm , Chu vi: ${clacRectangleP}cm</p>`
}
 
 /**
  * Bài 5
  * 
  * b1: input
  * Khai báo biến và gán giá trị 2 chữ số 
  * 
  * b2: xử lí
  * Đặt biến lấy số đơn vị Số / 10
  * Đặt biến lấy số hàng chục Số % 10
  * 
  * 
  * b3: output
  * cộng 2 biến lại với nhau và dùng Math.floor() để làm tròn xuống cho biến lấy Đơn vị 
  * trả kết quả bằng console.log
  * sủ dụng innerHTML để hiển thị lên trình duyệt
  */

function sumToNum() {
  var number = document.getElementById('two-num').value * 1
  console.log("Số nhập:",number)

  var SumNumberIntDozents = Math.floor(number / 10)
  var SumNumberIntUnits = Math.floor(number % 10)
  console.log("sumTwoNum: ",SumNumberIntDozents + SumNumberIntUnits)

  document.querySelector('.ResulttwoNumber').innerHTML = `<p>Tổng: ${SumNumberIntDozents + SumNumberIntUnits}</p>`
}